﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace AprendiendoCiencias
{
	[Activity (Label = "AcercaDe")]			
	public class AcercaDe : Activity
	{

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.AcercaDe);		
		}
			
		public override void OnBackPressed ()
		{
			terminarActivity ();
		}

		public void terminarActivity(){
			StartActivity (typeof(Menu));
			//base.OnBackPressed();
		}
	}
}

