﻿using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace AprendiendoCiencias
{
	[Activity (Label = "Tutorial")]			
	public class Tutorial : Activity
	{

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.Tutorial);		
		}

		public override void OnBackPressed ()
		{
			terminarActivity ();
		}

		//se tiene que pasar 3 veces una trivia para pasar de nivel (aprobar 3 trivias para pasar de nivel)
		//respuesta correcta 10 y incorrecta se restan 5
		//10 preguntas por nivel - 10 puntos por pregunta correcta

		public void terminarActivity(){
			StartActivity (typeof(Menu));
			//base.OnBackPressed();
		}
	}
}
