﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Telephony.Gsm;
using System.Threading.Tasks;
using System.Threading;
using Android.Content.PM;

/**
 * logica de la Activity que muestra el menu para ir a jugar o para ver los puntos
 * */
namespace AprendiendoCiencias
{
	[Activity (Label = "Trivia de Biología", ScreenOrientation = ScreenOrientation.Portrait)]			
	public class Menu : Activity
	{
		private ShareActionProvider mShareActionProvider;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView (Resource.Layout.Menu);

			ImageButton btnTrivia = FindViewById<ImageButton> (Resource.Id.btnJugar);
			ImageButton btnRanking = FindViewById<ImageButton> (Resource.Id.btnRanking);

			//ActionBar.SetHomeButtonEnabled(true);
			//ActionBar.SetDisplayHomeAsUpEnabled(true);

			btnTrivia.Click += delegate {
				StartActivity (typeof(Nivel));
			};

			btnRanking.Click += delegate {
				var intent = new Intent (this, typeof(Puntuacion));
				intent.PutExtra ("nivel", "1");
				StartActivity (intent);
			};				
				

		}


		/* con licencia se puede hacer de esta forma los listener de los botones
		[Java.Interop.Export("onClickTrivia")] // The value found in android:onClick attribute.
		public void onClickTrivia(View v)
		{
			var intent = new Intent (this, typeof(Nivel));
			intent.PutExtra ("nombre", "Mario Sanhueza");
			StartActivity (intent); 	
		}*/

		public override bool OnCreateOptionsMenu(IMenu menu)
		{
			MenuInflater.Inflate(Resource.Menu.actionbar_main, menu);
			MenuInflater.Inflate(Resource.Menu.menu_item_share, menu);
			// Locate MenuItem with ShareActionProvider
			IMenuItem item = menu.FindItem (Resource.Id.menu_item_share);
			// Fetch and store ShareActionProvider
		
			mShareActionProvider = (ShareActionProvider)item.ActionProvider; 

			mShareActionProvider.SetShareIntent(doShare());

			return true;
		}

		public Intent doShare() {
			// populate the share intent with data
			Intent intent = new Intent(Intent.ActionSend);
			intent.SetType("text/plain");
			intent.PutExtra(Intent.ExtraText, "Put whatever you want");
			return intent;
		}

		private void setShareIntent(Intent shareIntent)
		{
			if (mShareActionProvider != null) 
			{
				mShareActionProvider.SetShareIntent (shareIntent);
			}
		}


		public override bool OnOptionsItemSelected(IMenuItem item)
		{
			switch (item.ItemId)
			{
			/*case Resource.Id.cerrarSesion:
				ISharedPreferences pref = Application.Context.GetSharedPreferences ("UserInfo", FileCreationMode.Private);
				ISharedPreferencesEditor editor = pref.Edit ();
				editor.PutString ("idUsuario", String.Empty);
				editor.PutString ("nombre", String.Empty);
				editor.PutBoolean ("guardar", false);
				//editor.PutBoolean ("estaCargadaBD", false);
				editor.Apply ();

				StartActivity(typeof(Login));
				Finish(); 
				return true;*/

			case Resource.Id.salir:
				Finish ();
				return true;

			case Resource.Id.Acerca_de:
				var intent = new Intent (this, typeof(AcercaDe));
				StartActivity (intent);
				return true;

			case Resource.Id.Tutorial:
				var intentTutorial = new Intent (this, typeof(Tutorial));
				StartActivity (intentTutorial);
				return true;

				
			}
			return base.OnOptionsItemSelected(item);
		}

	}
}

