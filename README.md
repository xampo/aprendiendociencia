#  AprendiendoCiencias

Aplicación Android de "trivia de preguntas" para la asignatura de Biología, que busca apoyar el aprendizaje  a través tecnologías móviles (smartphones).
App que permite reforzar los conocimientos y habilidades de estudiantes de enseñanza media a través de cuestionarios del area de ciencias.

## Estructura ##
La estructura de archivos es básicamente la que nos ofrece Xamarin para el desarrollo en Android

- AprendiendoCiencias
    + Referencias 
    + Components
    + Paquetes
    + Assets 
    + Properties
        - AndroidManifest.xml
        - AssemblyInfo.cs
    + Resources
> todas estas carpetas son de interfaz de usuario
        - anim
        - drawable
        - drawable-hdpi
        - ...
        - layout
        - Menu
        - values
    + TablaAcciones
> Aqui se encuentran las acciones CRUD para acceder a la bd
    + TablasBD
> Aqui estan las estructuras para manipular las tablas de la BD
    + TablasItem
> Estructuras que se ocupan en la app 
    + otras clases...