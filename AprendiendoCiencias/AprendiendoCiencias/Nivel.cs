﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;
using Android.Content.PM;

/**
 *  Activity con la logica del menu para seleccionar un nivel y sus validaciones
 * */
using Android.Graphics;
using Android.Util;


namespace AprendiendoCiencias
{
	[Activity (Label = "Nivel", ScreenOrientation = ScreenOrientation.Portrait)]			
	public class Nivel : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.Nivel);
			SetTitle (Resource.String.niveles);

			ActionBar.SetHomeButtonEnabled(true);
			ActionBar.SetDisplayHomeAsUpEnabled(true);

			ImageButton btn1 = FindViewById<ImageButton> (Resource.Id.btn1);
			ImageButton btn2 = FindViewById<ImageButton> (Resource.Id.btn2);
			ImageButton btn3 = FindViewById<ImageButton> (Resource.Id.btn3);
			ImageButton btn4 = FindViewById<ImageButton> (Resource.Id.btn4);

			/*Typeface tf;
			try
			{
				tf = Typeface.CreateFromAsset(BaseContext.Assets, "Fonts/MIDDLE1.ttf");
				textView.Typeface=tf;
			}
			catch (Exception e)
			{
				Log.Error("ERROR", string.Format("Could not get Typeface: {0} Error: {1}", "FreeSchool.ttf", e));

			}*/



			NivelDesbloqueadoAccion n = new NivelDesbloqueadoAccion ();
			List<NivelDesbloqueadoItem> listaNivelesDesbloqueados = n.getNivelesDesbloqueados ();
			for (int i = 0; i < listaNivelesDesbloqueados.Count; i++) {
				NivelDesbloqueadoItem item = listaNivelesDesbloqueados [i];
				if (item.nivel == 1) {
					if(item.desbloqueado == true){
						btn1.SetBackgroundResource (Resource.Drawable.nivel1);
					}
					else{
						btn1.SetBackgroundResource (Resource.Drawable.nivel1_bloqueado);
					}
					btn1.Click += delegate {
						var intent = new Intent (this, typeof(Pregunta));
						intent.PutExtra ("nivel", "1");
						StartActivity (intent);
						Finish();
					};
				} else if (item.nivel == 2) {
					if(item.desbloqueado == true){
						btn2.SetBackgroundResource (Resource.Drawable.nivel2);
					}
					else{
						btn2.SetBackgroundResource (Resource.Drawable.nivel2_bloqueado);
					}
					btn2.Click += delegate {
						if(item.desbloqueado == true){
							var intent = new Intent (this, typeof(Pregunta));
							intent.PutExtra ("nivel", "2");
							StartActivity (intent);
							Finish();
						}
						else{
							PuntuacionAccion p = new PuntuacionAccion();
							int cant = 3 - p.getPuntuacionMore80("1");
							Toast.MakeText (this, "Debes aprobar "+ cant + " trivia(s) del nivel anterior para desbloquear el nivel 2", ToastLength.Long).Show();
						}
					};
				} else if (item.nivel == 3) {
					if(item.desbloqueado == true){
						btn3.SetBackgroundResource (Resource.Drawable.nivel3);
					}
					else{
						btn3.SetBackgroundResource (Resource.Drawable.nivel3_bloqueado);
					}
					btn3.Click += delegate {
						if(item.desbloqueado == true){
							var intent = new Intent (this, typeof(Pregunta));
							intent.PutExtra ("nivel", "3");
							StartActivity (intent);
							Finish();
						}
						else{
							PuntuacionAccion p = new PuntuacionAccion();
							int cant = 3 - p.getPuntuacionMore80("2");
							Toast.MakeText (this, "Debes aprobar "+ cant + " trivia(s) del nivel anterior para desbloquear el nivel 3", ToastLength.Long).Show();
						}
					};
				} else { // nivel == 4
					if(item.desbloqueado == true){
						btn4.SetBackgroundResource (Resource.Drawable.nivel4);
					}
					else{
						btn4.SetBackgroundResource (Resource.Drawable.nivel4_bloqueado);
					}
					btn4.Click += delegate {
						if(item.desbloqueado == true){
							var intent = new Intent (this, typeof(Pregunta));
							intent.PutExtra ("nivel", "4");
							StartActivity (intent);
							Finish();
						}
						else{
							PuntuacionAccion p = new PuntuacionAccion();
							int cant = 3 - p.getPuntuacionMore80("3");
							Toast.MakeText (this, "Debes aprobar "+ cant + " trivia(s) del nivel anterior para desbloquear el nivel 4", ToastLength.Long).Show();
						}
					};
				}
			}

			// Create your application here
		}

		public override bool OnOptionsItemSelected(IMenuItem item)
		{
			switch (item.ItemId)
			{

			case Android.Resource.Id.Home:
				Finish();
				return true;

			}
			return base.OnOptionsItemSelected(item);
		}


	}
}

