﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading;
using Android.Content.PM;

namespace AprendiendoCiencias
{
	/**
	 * Activity encargada de contener una pregunta a la vez en la interfaz del celular, 
	 * teniendo un listado de preguntas
	 * decide que tipo de pregunta se mostrará
	 * mantiene información de preguntas correctas, nivel y progreso del Quiz actual
	 * 
	 * */
	[Activity (Label = "Pregunta", ScreenOrientation = ScreenOrientation.Portrait)]	
	public class Pregunta : Activity
	{

		public static int numPregunta;
		public static string nivel;
		public static int buenas;
		public static int malas;
		public static bool esCorrecta;
		public static FragmentPreguntaA fragmentPreguntaA;//Fragment usado para las preguntas de tres opciones
		public static FragmentPreguntaB fragmentPreguntaB;//Fragment usado para las preguntas de ordenar una palabra
		public static FragmentPreguntaC fragmentPreguntaC;//Fragment usado para las preguntas de completacion
		public static bool preguntaA;
		public static int indicePregunta;
		public static List<ListadoPreguntaSolucionItem> listadoPreguntas;
		public static List<ImageView> listaBarraProgreso;
		public static int progreso;
		public static ImageView avance1;
		public static ImageView avance2;
		public static ImageView avance3;
		public static ImageView avance4;
		public static ImageView avance5;
		public static ImageView avance6;
		public static ImageView avance7;
		public static ImageView avance8;
		public static ImageView avance9;
		public static ImageView avance10;
		//public static ImageView iconoPuntaje;
		public static TextView puntaje;
		public static int puntos;
		/**
		 * Inicializador de la Activity
		 * uniendo la parte logica con la interfaz de usuario
		 * */
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.Pregunta);
			preguntaA = true;
			fragmentPreguntaA = new FragmentPreguntaA ();
			fragmentPreguntaB = new FragmentPreguntaB ();
			indicePregunta = 0;

			ActionBar.SetHomeButtonEnabled(true);
			ActionBar.SetDisplayHomeAsUpEnabled(true);

			nivel = Intent.GetStringExtra ("nivel") ?? "0";

			if (nivel.Equals ("1")) {
				SetTitle (Resource.String.preguntas_n1);
			} else if (nivel.Equals ("2")) {
				SetTitle (Resource.String.preguntas_n2);
			} else if (nivel.Equals ("3")) {
				SetTitle (Resource.String.preguntas_n3);
			} else {
				SetTitle (Resource.String.preguntas_n4);
			}

			Button btnPreguntaSiguiente = FindViewById<Button> (Resource.Id.btnPreguntaSiguiente);
			//ProgressBar pbPregunta = FindViewById<ProgressBar> (Resource.Id.pbPregunta);
			//TextView tvProgreso = FindViewById<TextView> (Resource.Id.tvProgreso);
			//TextView tvCorrectas = FindViewById<TextView> (Resource.Id.tvCorrectas);
			//TextView tvIncorrectas = FindViewById<TextView> (Resource.Id.tvIncorrectas);
			avance1 = FindViewById<ImageView> (Resource.Id.imageView1);
			avance2 = FindViewById<ImageView> (Resource.Id.imageView2);
			avance3 = FindViewById<ImageView> (Resource.Id.imageView3);
			avance4 = FindViewById<ImageView> (Resource.Id.imageView4);
			avance5 = FindViewById<ImageView> (Resource.Id.imageView5);
			avance6 = FindViewById<ImageView> (Resource.Id.imageView6);
			avance7 = FindViewById<ImageView> (Resource.Id.imageView7);
			avance8 = FindViewById<ImageView> (Resource.Id.imageView8);
			avance9 = FindViewById<ImageView> (Resource.Id.imageView9);
			avance10 = FindViewById<ImageView> (Resource.Id.imageView10);
			//iconoPuntaje = FindViewById<ImageView> (Resource.Id.imageView11);
			//iconoPuntaje.SetImageResource (Resource.Drawable.icono_puntaje);
			puntaje = FindViewById<TextView> (Resource.Id.textView1);
			puntos = 0;
			puntaje.Text = "" + 0;

			progreso = 0; 
			buenas = 0;
			malas = 0;

			desabilitarBarraAvance ();

			//tvCorrectas.Text = "CORRECTAS: " + buenas+"/10";
			//tvIncorrectas.Text = "INCORRECTAS: " + malas+"/2";
			//pbPregunta.Progress = progreso;
			//tvProgreso.Text = "Pregunta: " + (numPregunta + 1) + " / 10";



			int auxNivel = Int32.Parse(nivel);
			listadoPreguntas = getListadoPreguntaSolucion (auxNivel);

			//obtengo la respuesta correcta de la pregunta actual
			ListadoPreguntaSolucionItem preguntaActual = getPreguntaActual ();	
			string respuestaActual = obtenerRespuesta (preguntaActual.objListaRespuestas);


			//Decido si ocupo fragment A, B o C para la primera pregunta
		//	if (getTipoFragment (respuestaActual) == 1) {
				mostrarFragmentA ();
		//	}
		/*	else if(getTipoFragment (respuestaActual) == 2){
				mostrarFragmentB ();
			}
			else if(getTipoFragment (respuestaActual) == 3){
				//	mostrarFragmentC ();
				mostrarFragmentC ();
			}*/


			habilitarButtonSiguiente (false);
			btnPreguntaSiguiente.Click += delegate {
				indicePregunta++;
				if(malas >= 2 && buenas<8 && indicePregunta==10){
					cambiarActivity(false);
					return;
				}
				else{
					if(indicePregunta == 10){
						cambiarActivity(true);
						return;
					}
					preguntaActual = getPreguntaActual ();
					respuestaActual = obtenerRespuesta (preguntaActual.objListaRespuestas);
					habilitarButtonSiguiente (false);
					if(indicePregunta == 9){
						btnPreguntaSiguiente.Text = "Terminar";
					}
					if(indicePregunta < 10){        //Decido si ocupo fragment A o B
						//if (getTipoFragment (respuestaActual) == 1) {
							
							mostrarFragmentA ();
						//}
						//else if(getTipoFragment (respuestaActual) == 2){
						/*	mostrarFragmentB ();
						}
						else if(getTipoFragment (respuestaActual) == 3){
							mostrarFragmentC ();
						}*/

					}
				}

			};

		}



		/**
		 * Funcion que desabilita la barra de avance al comienzo de las preguntas
		 * */
		public void desabilitarBarraAvance(){

			avance1.SetImageResource (Resource.Drawable.circulod);
			avance2.SetImageResource (Resource.Drawable.circulod);
			avance3.SetImageResource (Resource.Drawable.circulod);
			avance4.SetImageResource (Resource.Drawable.circulod);
			avance5.SetImageResource (Resource.Drawable.circulod);
			avance6.SetImageResource (Resource.Drawable.circulod);
			avance7.SetImageResource (Resource.Drawable.circulod);
			avance8.SetImageResource (Resource.Drawable.circulod);
			avance9.SetImageResource (Resource.Drawable.circulod);
			avance10.SetImageResource (Resource.Drawable.circulod);
		}

		/**
		 * Funcion que se utiliza cuando se acaba un Quiz y lo lleva
		 * a la Activity de resultados.
		 * */
		public void cambiarActivity(bool aprobo){
			var intent = new Intent (this, typeof(MensajeResultado));
			intent.PutExtra ("nivel", nivel);
			intent.PutExtra ("buenas", buenas);
			intent.PutExtra ("malas", malas);
			intent.PutExtra("aprobo", aprobo);
			StartActivity (intent);
			Finish ();
		}

		/**
		 * Metodo void que actualiza la el contador de respuestas buenas y actualiza
		 * la interfaz para mostrar el resultado hasta el momento
		 * */
		public void incrementarRespuestasBuenas(){
			buenas++;

		
		}

		/**
		 * Metodo para actualizar el ProgressBar que muestra el progreso del Quiz
		 * se utiliza cada vez que se responda una pregunta
		 * */
		public void actualizarProgressBar(int Indicador){
			//ProgressBar pbPregunta = FindViewById<ProgressBar> (Resource.Id.pbPregunta);
			//TextView tvProgreso = FindViewById<TextView> (Resource.Id.tvProgreso);
			if (Indicador == 0) {//respuesta correcta
				if (indicePregunta == 0) {
					avance1.SetImageResource (Resource.Drawable.respuesta_correcta);
				}
				else if (indicePregunta == 1) {
					avance2.SetImageResource (Resource.Drawable.respuesta_correcta);
				}
				else if (indicePregunta == 2) {
					avance3.SetImageResource (Resource.Drawable.respuesta_correcta);
				}
				else if (indicePregunta == 3) {
					avance4.SetImageResource (Resource.Drawable.respuesta_correcta);
				}
				else if (indicePregunta == 4) {
					avance5.SetImageResource (Resource.Drawable.respuesta_correcta);
				}
				else if (indicePregunta == 5) {
					avance6.SetImageResource (Resource.Drawable.respuesta_correcta);
				}
				else if (indicePregunta == 6) {
					avance7.SetImageResource (Resource.Drawable.respuesta_correcta);
				}
				else if (indicePregunta == 7) {
					avance8.SetImageResource (Resource.Drawable.respuesta_correcta);
				}
				else if (indicePregunta == 8) {
					avance9.SetImageResource (Resource.Drawable.respuesta_correcta);
				}
				else if (indicePregunta == 9) {
					avance10.SetImageResource (Resource.Drawable.respuesta_correcta);
				}
			
			}
			else {//respuesta incorrecta
				if (indicePregunta == 0) {
					avance1.SetImageResource (Resource.Drawable.respuesta_incorrecta);
				}
				else if (indicePregunta == 1) {
					avance2.SetImageResource (Resource.Drawable.respuesta_incorrecta);
				}
				else if (indicePregunta == 2) {
					avance3.SetImageResource (Resource.Drawable.respuesta_incorrecta);
				}
				else if (indicePregunta == 3) {
					avance4.SetImageResource (Resource.Drawable.respuesta_incorrecta);
				}
				else if (indicePregunta == 4) {
					avance5.SetImageResource (Resource.Drawable.respuesta_incorrecta);
				}
				else if (indicePregunta == 5) {
					avance6.SetImageResource (Resource.Drawable.respuesta_incorrecta);
				}
				else if (indicePregunta == 6) {
					avance7.SetImageResource (Resource.Drawable.respuesta_incorrecta);
				}
				else if (indicePregunta == 7) {
					avance8.SetImageResource (Resource.Drawable.respuesta_incorrecta);
				}
				else if (indicePregunta == 8) {
					avance9.SetImageResource (Resource.Drawable.respuesta_incorrecta);
				}
				else if (indicePregunta == 9) {
					avance10.SetImageResource (Resource.Drawable.respuesta_incorrecta);
				}
			}

			progreso = indicePregunta + 1;
			Console.Write(indicePregunta);
			//pbPregunta.Progress = progreso;
			//tvProgreso.Text = "Pregunta: " + (indicePregunta + 1) + " / 10";

		}



		/**
		 *ActualizarPuntajesMostrado
		 *Este metodo actualiza el puntaje actual mostrado en la pantalla
		 */
		public void actualizarPuntajeMostrado()
		{
			
			puntos= (buenas*10) -(malas*3);
			puntaje.Text = ""+puntos;


		
		}

		/**
		 * Metodo void que actualiza la el contador de respuestas Malas y actualiza
		 * la interfaz para mostrar el resultado hasta el momento
		 * */
		public void incrementarRespuestasMalas(){
			malas++;
		}

		public int getBuenas(){
			return buenas;
		}

		public int getMalas(){
			return malas;
		}

		/**
		 * Metodo que retorna un ListadoPreguntaSolucionItem que es una 
		 * pregunta completa con su solucion 
		 * */
		public ListadoPreguntaSolucionItem getPreguntaActual(){
			ListadoPreguntaSolucionItem p = listadoPreguntas[indicePregunta];
			return p;
		}

		/**
		 * Metodo para habilitar el boton 'siguiente'
		 * este metodo debe ser usado cuando el usuario seleccione una opcion y esta listo
		 * para ir a la siguiente pregunta
		 * 
		 * habilitar:bool 
		 * si es true lo habilita de lo contrario se hace desaparecer
		 * */
		public void habilitarButtonSiguiente(bool habilitar){
			Button btnPreguntaSiguiente = FindViewById<Button> (Resource.Id.btnPreguntaSiguiente);
			if (habilitar) {
				btnPreguntaSiguiente.Enabled = true;
				btnPreguntaSiguiente.Visibility = ViewStates.Visible;
			} else {
				btnPreguntaSiguiente.Enabled = false;
				btnPreguntaSiguiente.Visibility = ViewStates.Gone;
			}
		}

		/**
		 * Metodo que se encarga de cambiar el texto del boton 'siguiente'
		 * 
		 * mensaje:string
		 * Texto que se mostrará en el boton
		 *
		 * */
		public void cambiarTextoBotonSiguiente(string mensaje){
			Button btnPreguntaSiguiente = FindViewById<Button> (Resource.Id.btnPreguntaSiguiente);
			btnPreguntaSiguiente.Text = mensaje;
		}

		/**
		 * Metodo para cargar el FragmentA en esta Activity
		 * */
		public void mostrarFragmentA(){
			FragmentTransaction fragmentTx = this.FragmentManager.BeginTransaction();
			fragmentPreguntaA = new FragmentPreguntaA();
			fragmentTx.Replace(Resource.Id.fragment_container, fragmentPreguntaA);
			//fragmentTx.AddToBackStack(null);
			fragmentTx.Commit();
		}
		/**
		 * Metodo para cargar el FragmentB en esta Activity
		 * */
		public void mostrarFragmentB(){
			FragmentTransaction fragmentTx = this.FragmentManager.BeginTransaction();
			fragmentPreguntaB = new FragmentPreguntaB();
			fragmentTx.Replace(Resource.Id.fragment_container, fragmentPreguntaB);
			//fragmentTx.AddToBackStack(null);
			fragmentTx.Commit();
		}

		/**
		 * Metodo para cargar el FragmentC en esta Activity
		 * */
		public void mostrarFragmentC(){
			FragmentTransaction fragmentTx = this.FragmentManager.BeginTransaction();
			fragmentPreguntaC = new FragmentPreguntaC();
			fragmentTx.Replace(Resource.Id.fragment_container, fragmentPreguntaC);
			//fragmentTx.AddToBackStack(null);
			fragmentTx.Commit();
		}
			
		/**
		 * Metodo para inicializar la Activity
		 * toma 10 preguntas aleatorias de la lista de preguntas
		 * usando solo numeros int como los identificadores de las preguntas a utilizar
		 * 
		 * return: Lista de enteros con los indices de las preguntas elegidas
		 * 
		 * input: n:int
		 * cantidad de preguntas
		 * 
		 * */
		public List<int> getDiezPreguntasAleatorias(int n){
			List<int> listaTotalPreguntas = new List<int>();
			int total = n;
			for(int i=1; i<=total; i++){
				listaTotalPreguntas.Add (i);
			}
			Random r = new Random(DateTime.Now.Millisecond);
			List<int> listaNumeros = new List<int>();
			for (int j = 0; j < 10; j++) {
				int numero = r.Next(0, listaTotalPreguntas.Count());
				listaNumeros.Add (listaTotalPreguntas[numero]);
				listaTotalPreguntas.RemoveAt (numero);
			}
			return listaNumeros;
		}
		/**
		 * Metodo que obtiene el listado de preguntas con sus soluciones desde 
		 * la base de datos
		 * este metodo utiliza getDiezPreguntasAleatorias para obtener aleatoriamente los
		 * ids de las preguntas en la base de datos
		 * return: estructura de preguntas con solucion
		 * input: nivel:int  nivel del quiz
		 * */
		public List<ListadoPreguntaSolucionItem> getListadoPreguntaSolucion(int nivel){
			PreguntaAccion p = new PreguntaAccion ();
			PreguntaSolucionAccion ps = new PreguntaSolucionAccion ();
			List<PreguntaItem> listaPreguntas = p.getPreguntasBD (nivel);
			int totalPreguntas = listaPreguntas.Count;
			List<int> numeros = getDiezPreguntasAleatorias (totalPreguntas);
			int totalNumeros = numeros.Count;
			List<ListadoPreguntaSolucionItem> resultado = new List<ListadoPreguntaSolucionItem> ();
			for (int i = 0; i < totalNumeros; i++) {
				int idBD = numeros [i]-1; // -1 porque los idBD comienzan del 1 en adelante
				PreguntaItem pregunta = listaPreguntas [idBD];
				List<PreguntaSolucionItem> listaPreguntaSolucion = Shuffle(ps.getPreguntasSolucionBD (pregunta.ID));
				ListadoPreguntaSolucionItem item = new ListadoPreguntaSolucionItem (pregunta, listaPreguntaSolucion);
				resultado.Add (item);
			}
			return resultado;
		}

		/**
		 * Metodo que hace una permutacion de una lista
		 * */
		public List<T> Shuffle<T>(List<T> list)
		{
			List<T> randomizedList = new List<T>();
			Random rnd = new Random(DateTime.Now.Millisecond);
			while (list.Count > 0)
			{
				int index = 1;
				for (int i = 0; i < 2; i++) {
					index = rnd.Next(0, list.Count);
				}
				randomizedList.Add(list[index]);
				list.RemoveAt(index);
			}
			return randomizedList;
		}

		/**
		 * Metodo que muestra una imagen en toast de android
		 * 
		 * input: idImagen:int id de la imagen de los recursos de la app
		 * */
		public void mostrarToastConImagen(int idImagen){
			Toast toast = new Toast(this);
			ImageView view = new ImageView(this); 
			view.SetImageResource(idImagen);
			toast.View = view;
			toast.Duration = ToastLength.Short;
			toast.Show();
		}

		/**
		 * Metodo que muestra un AlertDialog con el mensaje de advertencia
		 * se utiliza cuando se preciona el boton 'regresar'
		 * */
		public void mostrarMensajeAlerta(){
			AlertDialog.Builder alert = new AlertDialog.Builder (this);
			alert.SetCancelable(false);

			alert.SetTitle ("Advertencia");
			alert.SetMessage ("Si desea salir de la trivia no quedará registrado su progreso, debe terminarla para guardar su puntaje. ¿Desea salir de todas formas?");

			alert.SetPositiveButton ("Si", (senderAlert, args) => {
				StartActivity (typeof(Nivel));
				Finish ();
			} );

			alert.SetNegativeButton ("No", (senderAlert, args) => {

			} );

			RunOnUiThread (() => {
				alert.Show();
			} );
		}

		public override void OnBackPressed ()
		{
			mostrarMensajeAlerta ();
		}

		public override bool OnOptionsItemSelected(IMenuItem item)
		{
			switch (item.ItemId)
			{

			case Android.Resource.Id.Home:
				
				mostrarMensajeAlerta ();
				return true;

			}
			return base.OnOptionsItemSelected(item);
		}

		/**
		 * Funcion que retorna la respuesta correcta desde un listado de respuestas
		 * de una pregunta actual
		 * */
		public string obtenerRespuesta(List<PreguntaSolucionItem> lista){
			for(int i=0; i<lista.Count; i++){
				if(lista[i].esSolucion){
					return lista [i].solucion;
				}
			}
			return "";
		}

		/**
		 * Funcion que retorna 1, 2 o 3 si la respuesta es para el fragment A, B, C
		 * */
		public int getTipoFragment(string respuesta)
		{
			//tiene una sola palabra sin saber si tiene caracateres raros
			string[] palabras = respuesta.Split(' ');
			int npalabras = palabras.Count ();
			if (npalabras == 1) {
				//si es un numero Romano
				if (palabras [0].Equals ("I") ||
				    palabras [0].Equals ("II") ||
				    palabras [0].Equals ("III") ||
				    palabras [0].Equals ("IV") ||
					palabras [0].Equals ("I,") ||
					palabras [0].Equals ("II,") ||
					palabras [0].Equals ("III,") ||
					palabras [0].Equals ("IV,") ||
				    palabras [0].Equals ("Ninguna")) {
					return 1;
				} else if (Regex.IsMatch (respuesta, @"^[a-zA-ZáéíóúÁÉÍÓÚ]+$")) {
					return 2;
				}


			}
			else 
			{
				//numero Romano
				if (palabras [0].Equals ("I") ||
					palabras [0].Equals ("II") ||
					palabras [0].Equals ("III") ||
					palabras [0].Equals ("IV") ||
					palabras [0].Equals ("I,") ||
					palabras [0].Equals ("II,") ||
					palabras [0].Equals ("III,") ||
					palabras [0].Equals ("IV,") ||
					palabras [0].Equals ("Ninguna")) {
					return 1;
				} 
				//palabras humanas de mas de una letra
				else if ((npalabras > 1) && Regex.IsMatch (palabras[0], @"^[a-zA-ZáéíóúÁÉÍÓÚ]+$")) {
					return 3;
				}


			}
			return 1;

		}

	}

}

